
<?php
if ( 'page' == get_option( 'show_on_front' ) && ( '' != get_option( 'page_for_posts' ) ) && $wp_query->get_queried_object_id() == get_option( 'page_for_posts' ) ) :
get_header(); 
else:
get_header('home'); 
endif;
?>

<?php while(have_posts()) : the_post(); ?>
<section class="single-post">
    <div class="container">

        <div class="single-post-banner">
            <h1 class="single-post-title">
                <?php the_title(); ?>
            </h1>
            <img src="<?php echo get_field('single_image_banner', get_the_ID()); ?>">
        </div>

        <div class="row">
            <div class="col-md-9 single-post-info">

                <?php the_content(); ?>

                <section class="home-section-4">
                    <?php
                        echo do_shortcode('
                            [ms_modal title="" size="middle" showfooter="no" class="pop-up-dk" id=""]
                                [ms_modal_anchor_text]<input style="background: #1e73be; color: white; border: none; padding: 8px 25px; font-family: Arial;" type="submit" value="Ứng Tuyển" />
                                [/ms_modal_anchor_text]
                                [ms_modal_content]
                                    [contact-form-7 id="14" title="contact-popup"]
                                [/ms_modal_content]
                            [/ms_modal]
                        ');
                    ?>
                </section>
            </div>

            <div class="col-md-3 single-post-sidebar">
                <?php dynamic_sidebar('single_post'); ?>
            </div>
        </div>

    </div>
</section>
<?php endwhile; ?>

<?php get_footer();?>
